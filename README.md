## Introduction

This is a project testing code from Eclipx Group. Here is back-end repo with Nodejs. 

## Services
```
Nodejs
ExpressJS
Axios
```

## Installation

Use [npm] package. From root
```
npm install
npm start
```

## Workflow

You need to run the back-end server before do the test Check user in the front-end.

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('express-jwt');
const jwks = require('jwks-rsa');
const dotenv = require('dotenv')
const axios = require('axios')

const app = express();

const dataObj = {
  domain: "https://eclipx-coding-test.au.auth0.com"
}

dotenv.config();
app.use(bodyParser.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));



var jwtCheck = jwt({
    secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `${dataObj.domain}/.well-known/jwks.json`
  }),
  audience: 'https://eclipx-test/api',
  issuer: `${dataObj.domain}/`,
  algorithms: ['RS256']
});


const getManagementApi =  (req, res, next) => {
  axios
    .post(`${dataObj.domain}/oauth/token`, {
      grant_type: "client_credentials",
      client_id: process.env.AUTH0_CLIENT_ID,
      client_secret:process.env.AUTH0_CLIENT_SECRET,
      audience: `${dataObj.domain}/api/v2/`,
    })
    .then((response) => {
      req.managementApiToken = response.data.access_token
    })
    .finally(() =>    next());
};

app.get("/api/admin_only", jwtCheck, getManagementApi, (req, res) => {
  axios
    .get(
      `${dataObj.domain}/api/v2/users/${req.user.sub}/roles`,
      {
        headers: {
          Authorization: `Bearer ${req.managementApiToken}`,
        },
      }
    )
    .then((response) => {
      const roles = response.data;

      let isAdmin = false;

      roles.map((role) => {
        if (role.name == "admin") {
          isAdmin = true;
        }
      });
      
      //if user have admin role
      if (isAdmin) {
        res.status(200).send('This account authorized to fetch this endpoint');
      }
      res.send(401);
    })
    
    .catch(error => {
      res.send(500)
    });
});

// listen on the port
app.listen(process.env.PORT);

